from collections import defaultdict
from pprint import pprint

import requests

BASE = 'http://www.catalogo.beniculturali.it/opendata/?q='
START = BASE + 'api/3/action/current_package_list_with_resources'
RESOURCE = BASE + 'api/action/datastore/search.json'

r = requests.get(START)

counts = defaultdict(int)

for dataset in r.json()['result']:
    for resource in dataset['resources']:
        if resource['title'].endswith(('RA 2.00', 'RA 3.00', 'RA3.00')):
            r = requests.get(RESOURCE, params={'resource_id': resource['id'],
                                               'limit': 1})
            result = r.json()['result']
            counts[dataset['title']] += int(result['total'])

for regione, num in sorted(counts.items(), key=lambda x: x[1]):
    print('{},{}'.format(' '.join(regione.split()[1:]), num))
